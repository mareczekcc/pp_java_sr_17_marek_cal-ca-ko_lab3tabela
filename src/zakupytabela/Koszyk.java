// **********************************************************************
//   Koszyk.java
//
//   Reprezentuje koszyk zakupowy jako tablica przedmiotow (Items)
// **********************************************************************
package zakupytabela;

import zakupytabela.Item;
import java.text.NumberFormat;

public class Koszyk {

    public int iloscRzeczy;       // ilosc rzeczy w koszyku
    public double cenaCalkowita;  // calkowita cena przedmiotow w koszyku
    public int pojemnosc;         // biezaca pojemnosc koszyka
    public Item koszyk[];

    // -----------------------------------------------------------
    // tworzy pusty koszyk o pojemnosci 5 przedmiotow.
    // -----------------------------------------------------------
    public Koszyk() {
        pojemnosc = 5;
        iloscRzeczy = 0;
        cenaCalkowita = 0.0;
        koszyk = new Item[pojemnosc];
    }

    // -------------------------------------------------------
    //  Dodaje rzecz do koszyka.
    // -------------------------------------------------------
    public void dodajDoKoszyka(String nazwaRzeczy, double cena, int ilosc) {

        if (iloscRzeczy >= pojemnosc) {
            powiekszRozmiar();
        }
        koszyk[iloscRzeczy] = new Item(nazwaRzeczy, cena, ilosc);
        iloscRzeczy++;
        cenaCalkowita += cena*ilosc;

    }

    // -------------------------------------------------------
    //  Zwraca zawartosc koszyka wraz z podsumowaniem.
    // -------------------------------------------------------
    public String toString() {
        NumberFormat fmt = NumberFormat.getCurrencyInstance();
System.out.println("Wypisujemy!");
        String zawartosc = "\nKoszyk\n";
        zawartosc += "\nRzecz\t\tCena jednostkowa\tIlosc\tSuma\n";

        for (int i = 0; i < iloscRzeczy; i++) {
            zawartosc += koszyk[i].toString() + "\n";
        }

        zawartosc += "\nTotal Price: " + fmt.format(cenaCalkowita);
        zawartosc += "\n";

        return zawartosc;
    }

    // ---------------------------------------------------------
    //  Zwieksza pojemnosc koszyka o 3
    // ---------------------------------------------------------
    private void powiekszRozmiar() {

        Item nowyKoszyk[] = new Item[pojemnosc + 3];

        for (int i = 0; i < pojemnosc; i++) {
            nowyKoszyk[i] = koszyk[i];
        }

        this.pojemnosc += 3;

        this.koszyk = nowyKoszyk;
        System.out.println("Powiekszono koszyk, aktualna pojemnosc: "
                +this.pojemnosc +", aktualna ilosc rzeczy w koszyku: " 
                +this.iloscRzeczy);
    }

}
