package zakupytabela;

import java.util.Scanner;
import zakupytabela.Koszyk;

public class ZakupyTabela {

    public static void main(String[] args) {
        String kontynuujZakupy;
        Koszyk nowyKoszyk = new Koszyk();

        Scanner klawiatura = new Scanner(System.in);

        String nazwaRzeczy;
        double cenaRzeczy;
        int ilosc;

        kontynuujZakupy = "t";

        while (kontynuujZakupy.equals("t")) {
            System.out.println("Podaj nazwe rzeczy: ");
            nazwaRzeczy = klawiatura.next();

            System.out.println("Podaj cene jednostkowa: ");
            cenaRzeczy = klawiatura.nextDouble();

            System.out.println("Podaj ilosc: ");
            ilosc = klawiatura.nextInt();

            nowyKoszyk.dodajDoKoszyka(nazwaRzeczy, cenaRzeczy, ilosc);

            System.out.println("Kontynuowac zakupy (t/n)? ");
            kontynuujZakupy = klawiatura.next();

        }

        System.out.println(nowyKoszyk.toString());


    }

}
