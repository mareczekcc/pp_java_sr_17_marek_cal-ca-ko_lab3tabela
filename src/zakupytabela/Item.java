// ***************************************************************
//   Item.java
//
//   Reprezentuje rzecz w koszyku.
// ***************************************************************

package zakupytabela;

import java.text.NumberFormat;

public class Item
{
    private String nazwa;
    private double cena;
    private int ilosc;


    // -------------------------------------------------------
    //  Tworzy nowa rzecz z zadanych parametrow.
    // -------------------------------------------------------
    public Item (String nazwaRzeczy, double cenaRzeczy, int iloscZakupionych)
    {
	nazwa = nazwaRzeczy;
	cena = cenaRzeczy;
	ilosc = iloscZakupionych;
    }


    // -------------------------------------------------------
    //   Zwraca lancuch znakowy z informacjami o przedmiocie
    // -------------------------------------------------------
    public String toString ()
    {
	NumberFormat fmt = NumberFormat.getCurrencyInstance();

	return (nazwa + "\t\t" + fmt.format(cena) + "\t\t\t" + ilosc + "\t" //Dodalem taba!
		+ fmt.format(cena*ilosc));
    }

    // -------------------------------------------------
    //   Zwraca cene jednostkowa przedmiotu
    // -------------------------------------------------
    public double getCena()
    {
	return cena;
    }

    // -------------------------------------------------
    //   Zwraca nazwe przedmiotu
    // -------------------------------------------------
    public String getNazwa()
    {
	return nazwa;
    }

    // -------------------------------------------------
    //   Zwraca ilosc rzeczy
    // -------------------------------------------------
    public int getIlosc()
    {
	return ilosc;
    }
}  
